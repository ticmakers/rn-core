[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md)

# External module: "Validation/index.d"

## Index

### Classes

* [ValidationComponent](../classes/_validation_index_d_.validationcomponent.md)

### Interfaces

* [IValidationError](../interfaces/_validation_index_d_.ivalidationerror.md)
* [IValidationInputRules](../interfaces/_validation_index_d_.ivalidationinputrules.md)
* [IValidationLocale](../interfaces/_validation_index_d_.ivalidationlocale.md)
* [IValidationMessages](../interfaces/_validation_index_d_.ivalidationmessages.md)
* [IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md)
* [IValidationRules](../interfaces/_validation_index_d_.ivalidationrules.md)

### Type aliases

* [TypeValidationValue](_validation_index_d_.md#typevalidationvalue)

---

## Type aliases

<a id="typevalidationvalue"></a>

###  TypeValidationValue

**Ƭ TypeValidationValue**: *`false` \| `string` \| `number` \| `Date` \| `object` \| `any`[] \| `null` \| `undefined`*

*Defined in Validation/index.d.ts:6*

Type to define the value of the Validation component

___

