[@ticmakers-react-native/core](../README.md) > ["Validation/locales/fr"](../modules/_validation_locales_fr_.md)

# External module: "Validation/locales/fr"

## Index

### Object literals

* [fr](_validation_locales_fr_.md#fr)

---

## Object literals

<a id="fr"></a>

### `<Const>` fr

**fr**: *`object`*

*Defined in Validation/locales/fr.ts:1*

<a id="fr.date"></a>

####  date

**● date**: *`string`* = "Le champ "{0}" doit correspondre à une date valide ({1})."

*Defined in Validation/locales/fr.ts:2*

___
<a id="fr.email"></a>

####  email

**● email**: *`string`* = "Le champ "{0}" doit être une adresse email valide."

*Defined in Validation/locales/fr.ts:3*

___
<a id="fr.maxlength"></a>

####  maxLength

**● maxLength**: *`string`* = "Le nombre de caractère du champ "{0}" doit être inférieur à {1}."

*Defined in Validation/locales/fr.ts:4*

___
<a id="fr.minlength"></a>

####  minLength

**● minLength**: *`string`* = "Le nombre de caractère du champ "{0}" doit être supérieur à {1}."

*Defined in Validation/locales/fr.ts:5*

___
<a id="fr.numbers"></a>

####  numbers

**● numbers**: *`string`* = "Le champ "{0}" doit être un nombre valide."

*Defined in Validation/locales/fr.ts:6*

___
<a id="fr.required"></a>

####  required

**● required**: *`string`* = "Le champ "{0}" est obligatoire."

*Defined in Validation/locales/fr.ts:7*

___

___

