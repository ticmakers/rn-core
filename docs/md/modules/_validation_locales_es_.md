[@ticmakers-react-native/core](../README.md) > ["Validation/locales/es"](../modules/_validation_locales_es_.md)

# External module: "Validation/locales/es"

## Index

### Object literals

* [es](_validation_locales_es_.md#es)

---

## Object literals

<a id="es"></a>

### `<Const>` es

**es**: *`object`*

*Defined in Validation/locales/es.ts:1*

<a id="es.date"></a>

####  date

**● date**: *`string`* = "El campo "{0}" debe ser una fecha válida ({1})."

*Defined in Validation/locales/es.ts:2*

___
<a id="es.email"></a>

####  email

**● email**: *`string`* = "El campo "{0}" debe ser un correo electrónico válido."

*Defined in Validation/locales/es.ts:3*

___
<a id="es.maxlength"></a>

####  maxLength

**● maxLength**: *`string`* = "El campo "{0}" debe ser una longitud inferior a {1}."

*Defined in Validation/locales/es.ts:4*

___
<a id="es.minlength"></a>

####  minLength

**● minLength**: *`string`* = "El campo "{0}" debe ser una longitud superior a {1}."

*Defined in Validation/locales/es.ts:5*

___
<a id="es.numbers"></a>

####  numbers

**● numbers**: *`string`* = "El campo "{0}" debe ser un numero válido."

*Defined in Validation/locales/es.ts:6*

___
<a id="es.required"></a>

####  required

**● required**: *`string`* = "El campo "{0}" es obligatorio."

*Defined in Validation/locales/es.ts:7*

___

___

