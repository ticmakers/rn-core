[@ticmakers-react-native/core](../README.md) > ["Validation/locales/en"](../modules/_validation_locales_en_.md)

# External module: "Validation/locales/en"

## Index

### Object literals

* [en](_validation_locales_en_.md#en)

---

## Object literals

<a id="en"></a>

### `<Const>` en

**en**: *`object`*

*Defined in Validation/locales/en.ts:1*

<a id="en.date"></a>

####  date

**● date**: *`string`* = "The field "{0}" must be a valid date ({1})."

*Defined in Validation/locales/en.ts:2*

___
<a id="en.email"></a>

####  email

**● email**: *`string`* = "The field "{0}" must be a valid email address."

*Defined in Validation/locales/en.ts:3*

___
<a id="en.maxlength"></a>

####  maxLength

**● maxLength**: *`string`* = "The field "{0}" length must be lower than {1}."

*Defined in Validation/locales/en.ts:4*

___
<a id="en.minlength"></a>

####  minLength

**● minLength**: *`string`* = "The field "{0}" length must be greater than {1}."

*Defined in Validation/locales/en.ts:5*

___
<a id="en.numbers"></a>

####  numbers

**● numbers**: *`string`* = "The field "{0}" must be a valid number."

*Defined in Validation/locales/en.ts:6*

___
<a id="en.required"></a>

####  required

**● required**: *`string`* = "The field "{0}" is required."

*Defined in Validation/locales/en.ts:7*

___

___

