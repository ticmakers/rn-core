[@ticmakers-react-native/core](../README.md) > ["Validation/locales/fa"](../modules/_validation_locales_fa_.md)

# External module: "Validation/locales/fa"

## Index

### Object literals

* [fa](_validation_locales_fa_.md#fa)

---

## Object literals

<a id="fa"></a>

### `<Const>` fa

**fa**: *`object`*

*Defined in Validation/locales/fa.ts:1*

<a id="fa.date"></a>

####  date

**● date**: *`string`* = "فیلد "{0}" باید یک تاریخ ({1}) باشد."

*Defined in Validation/locales/fa.ts:2*

___
<a id="fa.email"></a>

####  email

**● email**: *`string`* = "فیلد "{0}" باید یک آدرس ایمیل باشد."

*Defined in Validation/locales/fa.ts:3*

___
<a id="fa.maxlength"></a>

####  maxLength

**● maxLength**: *`string`* = "طول فیلد "{0}" باید کمتر از "{1}" باشد."

*Defined in Validation/locales/fa.ts:4*

___
<a id="fa.minlength"></a>

####  minLength

**● minLength**: *`string`* = "طول فیلد "{0}" باید بیشتر از "{1}" باشد."

*Defined in Validation/locales/fa.ts:5*

___
<a id="fa.numbers"></a>

####  numbers

**● numbers**: *`string`* = "فیلد "{0}" باید یک عدد باشد."

*Defined in Validation/locales/fa.ts:6*

___
<a id="fa.required"></a>

####  required

**● required**: *`string`* = "فیلد "{0}" نباید خالی باشد."

*Defined in Validation/locales/fa.ts:7*

___

___

