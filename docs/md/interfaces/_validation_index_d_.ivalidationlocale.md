[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md) > [IValidationLocale](../interfaces/_validation_index_d_.ivalidationlocale.md)

# Interface: IValidationLocale

Interface to define the validation locale messages

*__interface__*: IValidationLocale

## Hierarchy

**IValidationLocale**

## Index

### Properties

* [en](_validation_index_d_.ivalidationlocale.md#en)
* [es](_validation_index_d_.ivalidationlocale.md#es)
* [fa](_validation_index_d_.ivalidationlocale.md#fa)
* [fr](_validation_index_d_.ivalidationlocale.md#fr)

---

## Properties

<a id="en"></a>

### `<Optional>` en

**● en**: *[IValidationMessages](_validation_index_d_.ivalidationmessages.md)*

*Defined in Validation/index.d.ts:62*

___
<a id="es"></a>

### `<Optional>` es

**● es**: *[IValidationMessages](_validation_index_d_.ivalidationmessages.md)*

*Defined in Validation/index.d.ts:63*

___
<a id="fa"></a>

### `<Optional>` fa

**● fa**: *[IValidationMessages](_validation_index_d_.ivalidationmessages.md)*

*Defined in Validation/index.d.ts:65*

___
<a id="fr"></a>

### `<Optional>` fr

**● fr**: *[IValidationMessages](_validation_index_d_.ivalidationmessages.md)*

*Defined in Validation/index.d.ts:64*

___

