[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md) > [IValidationError](../interfaces/_validation_index_d_.ivalidationerror.md)

# Interface: IValidationError

Interface to define the validation message error

*__interface__*: IValidationError

## Hierarchy

**IValidationError**

## Index

### Properties

* [message](_validation_index_d_.ivalidationerror.md#message)
* [rule](_validation_index_d_.ivalidationerror.md#rule)

---

## Properties

<a id="message"></a>

###  message

**● message**: *`string`*

*Defined in Validation/index.d.ts:74*

___
<a id="rule"></a>

###  rule

**● rule**: *`string`*

*Defined in Validation/index.d.ts:73*

___

