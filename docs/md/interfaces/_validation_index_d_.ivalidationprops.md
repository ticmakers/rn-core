[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md) > [IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md)

# Interface: IValidationProps

Interface to define the props of the Validation component

*__interface__*: IValidationProps

## Hierarchy

**IValidationProps**

## Index

### Properties

* [deviceLocale](_validation_index_d_.ivalidationprops.md#devicelocale)
* [errors](_validation_index_d_.ivalidationprops.md#errors)
* [fieldName](_validation_index_d_.ivalidationprops.md#fieldname)
* [messages](_validation_index_d_.ivalidationprops.md#messages)
* [rules](_validation_index_d_.ivalidationprops.md#rules)

---

## Properties

<a id="devicelocale"></a>

### `<Optional>` deviceLocale

**● deviceLocale**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:88*

Define de locale to show the messages error

*__type__*: {string}

*__memberof__*: IValidationProps

*__default__*: en

___
<a id="errors"></a>

### `<Optional>` errors

**● errors**: *[IValidationError](_validation_index_d_.ivalidationerror.md)[]*

*Defined in Validation/index.d.ts:95*

Define a list of errors

*__type__*: {IValidationProps}

*__memberof__*: IValidationProps

___
<a id="fieldname"></a>

### `<Optional>` fieldName

**● fieldName**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:102*

Define the name of the field

*__type__*: {string}

*__memberof__*: IValidationProps

___
<a id="messages"></a>

### `<Optional>` messages

**● messages**: *`undefined` \| `object`*

*Defined in Validation/index.d.ts:109*

Define the messages error to show

*__type__*: {object}

*__memberof__*: IValidationProps

___
<a id="rules"></a>

### `<Optional>` rules

**● rules**: *`undefined` \| `object`*

*Defined in Validation/index.d.ts:116*

Define the rules methods by default

*__type__*: {object}

*__memberof__*: IValidationProps

___

