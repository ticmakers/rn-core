[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md) > [IValidationRules](../interfaces/_validation_index_d_.ivalidationrules.md)

# Interface: IValidationRules

Interface to define the validation rules

*__interface__*: IValidationRules

## Hierarchy

**IValidationRules**

## Index

### Properties

* [date](_validation_index_d_.ivalidationrules.md#date)
* [email](_validation_index_d_.ivalidationrules.md#email)
* [maxLength](_validation_index_d_.ivalidationrules.md#maxlength)
* [minLength](_validation_index_d_.ivalidationrules.md#minlength)
* [numbers](_validation_index_d_.ivalidationrules.md#numbers)
* [required](_validation_index_d_.ivalidationrules.md#required)

---

## Properties

<a id="date"></a>

### `<Optional>` date

**● date**: *`undefined` \| `function`*

*Defined in Validation/index.d.ts:21*

___
<a id="email"></a>

### `<Optional>` email

**● email**: *`RegExp` \| `string`*

*Defined in Validation/index.d.ts:22*

___
<a id="maxlength"></a>

### `<Optional>` maxLength

**● maxLength**: *`undefined` \| `function`*

*Defined in Validation/index.d.ts:23*

___
<a id="minlength"></a>

### `<Optional>` minLength

**● minLength**: *`undefined` \| `function`*

*Defined in Validation/index.d.ts:24*

___
<a id="numbers"></a>

### `<Optional>` numbers

**● numbers**: *`RegExp` \| `string`*

*Defined in Validation/index.d.ts:25*

___
<a id="required"></a>

### `<Optional>` required

**● required**: *`undefined` \| `function`*

*Defined in Validation/index.d.ts:26*

___

