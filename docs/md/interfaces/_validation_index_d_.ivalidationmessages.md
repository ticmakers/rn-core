[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md) > [IValidationMessages](../interfaces/_validation_index_d_.ivalidationmessages.md)

# Interface: IValidationMessages

Interface to define the validation messages error

*__interface__*: IValidationMessages

## Hierarchy

**IValidationMessages**

## Index

### Properties

* [date](_validation_index_d_.ivalidationmessages.md#date)
* [email](_validation_index_d_.ivalidationmessages.md#email)
* [maxLength](_validation_index_d_.ivalidationmessages.md#maxlength)
* [minLength](_validation_index_d_.ivalidationmessages.md#minlength)
* [numbers](_validation_index_d_.ivalidationmessages.md#numbers)
* [pattern](_validation_index_d_.ivalidationmessages.md#pattern)
* [required](_validation_index_d_.ivalidationmessages.md#required)

---

## Properties

<a id="date"></a>

### `<Optional>` date

**● date**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:34*

___
<a id="email"></a>

### `<Optional>` email

**● email**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:35*

___
<a id="maxlength"></a>

### `<Optional>` maxLength

**● maxLength**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:36*

___
<a id="minlength"></a>

### `<Optional>` minLength

**● minLength**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:37*

___
<a id="numbers"></a>

### `<Optional>` numbers

**● numbers**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:38*

___
<a id="pattern"></a>

### `<Optional>` pattern

**● pattern**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:39*

___
<a id="required"></a>

### `<Optional>` required

**● required**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:40*

___

