[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md) > [IValidationInputRules](../interfaces/_validation_index_d_.ivalidationinputrules.md)

# Interface: IValidationInputRules

Interface to define the validation input rules

*__interface__*: IValidationInputRules

## Hierarchy

**IValidationInputRules**

## Index

### Properties

* [date](_validation_index_d_.ivalidationinputrules.md#date)
* [email](_validation_index_d_.ivalidationinputrules.md#email)
* [maxLength](_validation_index_d_.ivalidationinputrules.md#maxlength)
* [minLength](_validation_index_d_.ivalidationinputrules.md#minlength)
* [numbers](_validation_index_d_.ivalidationinputrules.md#numbers)
* [pattern](_validation_index_d_.ivalidationinputrules.md#pattern)
* [required](_validation_index_d_.ivalidationinputrules.md#required)

---

## Properties

<a id="date"></a>

### `<Optional>` date

**● date**: *`boolean` \| `string` \| `RegExp`*

*Defined in Validation/index.d.ts:48*

___
<a id="email"></a>

### `<Optional>` email

**● email**: *`boolean` \| `RegExp`*

*Defined in Validation/index.d.ts:49*

___
<a id="maxlength"></a>

### `<Optional>` maxLength

**● maxLength**: *`undefined` \| `number`*

*Defined in Validation/index.d.ts:50*

___
<a id="minlength"></a>

### `<Optional>` minLength

**● minLength**: *`undefined` \| `number`*

*Defined in Validation/index.d.ts:51*

___
<a id="numbers"></a>

### `<Optional>` numbers

**● numbers**: *`boolean` \| `RegExp`*

*Defined in Validation/index.d.ts:52*

___
<a id="pattern"></a>

### `<Optional>` pattern

**● pattern**: *`RegExp`*

*Defined in Validation/index.d.ts:53*

___
<a id="required"></a>

### `<Optional>` required

**● required**: *`undefined` \| `true`*

*Defined in Validation/index.d.ts:54*

___

