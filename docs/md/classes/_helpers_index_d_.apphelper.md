[@ticmakers-react-native/core](../README.md) > ["Helpers/index.d"](../modules/_helpers_index_d_.md) > [AppHelper](../classes/_helpers_index_d_.apphelper.md)

# Class: AppHelper

Class to define the app helper

*__class__*: AppHelper

## Hierarchy

**AppHelper**

## Index

### Methods

* [isClassComponent](_helpers_index_d_.apphelper.md#isclasscomponent)
* [isComponent](_helpers_index_d_.apphelper.md#iscomponent)
* [isCompositeElement](_helpers_index_d_.apphelper.md#iscompositeelement)
* [isDOMElement](_helpers_index_d_.apphelper.md#isdomelement)
* [isElement](_helpers_index_d_.apphelper.md#iselement)
* [isFunctionComponent](_helpers_index_d_.apphelper.md#isfunctioncomponent)
* [isInstanceComponent](_helpers_index_d_.apphelper.md#isinstancecomponent)
* [isReactComponent](_helpers_index_d_.apphelper.md#isreactcomponent)

---

## Methods

<a id="isclasscomponent"></a>

###  isClassComponent

▸ **isClassComponent**(component: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:12*

Method to check if an object is a class component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="iscomponent"></a>

###  isComponent

▸ **isComponent**(component: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:68*

Method to check if an object is a component valid

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="iscompositeelement"></a>

###  isCompositeElement

▸ **isCompositeElement**(element: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:60*

Method to check if an object is a composite element component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| element | `any` |  \- |

**Returns:** `boolean`

___
<a id="isdomelement"></a>

###  isDOMElement

▸ **isDOMElement**(element: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:52*

Method to check if an object is a DOM element component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| element | `any` |  \- |

**Returns:** `boolean`

___
<a id="iselement"></a>

###  isElement

▸ **isElement**(element: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:44*

Method to check if an object is an element component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| element | `any` |  \- |

**Returns:** `boolean`

___
<a id="isfunctioncomponent"></a>

###  isFunctionComponent

▸ **isFunctionComponent**(component: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:20*

Method to check if an object is a function component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="isinstancecomponent"></a>

###  isInstanceComponent

▸ **isInstanceComponent**(component: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:28*

Method to check if an object is a instance component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="isreactcomponent"></a>

###  isReactComponent

▸ **isReactComponent**(component: *`any`*): `boolean`

*Defined in Helpers/index.d.ts:36*

Method to check if an object is a react component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___

