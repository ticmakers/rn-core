[@ticmakers-react-native/core](../README.md) > ["Validation/index.d"](../modules/_validation_index_d_.md) > [ValidationComponent](../classes/_validation_index_d_.validationcomponent.md)

# Class: ValidationComponent

Component base to add validations

*__class__*: ValidationComponent

*__extends__*: {(React.Component<IValidationProps & PP, SS>)}

## Type parameters
#### PP 
#### SS 
#### SS 
## Hierarchy

 `Component`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`, `SS`>

**↳ ValidationComponent**

## Index

### Constructors

* [constructor](_validation_index_d_.validationcomponent.md#constructor)

### Properties

* [context](_validation_index_d_.validationcomponent.md#context)
* [errors](_validation_index_d_.validationcomponent.md#errors)
* [fieldName](_validation_index_d_.validationcomponent.md#fieldname)
* [locale](_validation_index_d_.validationcomponent.md#locale)
* [messages](_validation_index_d_.validationcomponent.md#messages)
* [props](_validation_index_d_.validationcomponent.md#props)
* [refs](_validation_index_d_.validationcomponent.md#refs)
* [rules](_validation_index_d_.validationcomponent.md#rules)
* [state](_validation_index_d_.validationcomponent.md#state)
* [value](_validation_index_d_.validationcomponent.md#value)
* [contextType](_validation_index_d_.validationcomponent.md#contexttype)

### Methods

* [UNSAFE_componentWillMount](_validation_index_d_.validationcomponent.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_validation_index_d_.validationcomponent.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_validation_index_d_.validationcomponent.md#unsafe_componentwillupdate)
* [__processProps](_validation_index_d_.validationcomponent.md#__processprops)
* [_addError](_validation_index_d_.validationcomponent.md#_adderror)
* [_checkRules](_validation_index_d_.validationcomponent.md#_checkrules)
* [_defaultMessages](_validation_index_d_.validationcomponent.md#_defaultmessages)
* [_defaultRules](_validation_index_d_.validationcomponent.md#_defaultrules)
* [_resetErrors](_validation_index_d_.validationcomponent.md#_reseterrors)
* [componentDidCatch](_validation_index_d_.validationcomponent.md#componentdidcatch)
* [componentDidMount](_validation_index_d_.validationcomponent.md#componentdidmount)
* [componentDidUpdate](_validation_index_d_.validationcomponent.md#componentdidupdate)
* [componentWillMount](_validation_index_d_.validationcomponent.md#componentwillmount)
* [componentWillReceiveProps](_validation_index_d_.validationcomponent.md#componentwillreceiveprops)
* [componentWillUnmount](_validation_index_d_.validationcomponent.md#componentwillunmount)
* [componentWillUpdate](_validation_index_d_.validationcomponent.md#componentwillupdate)
* [forceUpdate](_validation_index_d_.validationcomponent.md#forceupdate)
* [getError](_validation_index_d_.validationcomponent.md#geterror)
* [getSnapshotBeforeUpdate](_validation_index_d_.validationcomponent.md#getsnapshotbeforeupdate)
* [hasError](_validation_index_d_.validationcomponent.md#haserror)
* [hasErrors](_validation_index_d_.validationcomponent.md#haserrors)
* [isValid](_validation_index_d_.validationcomponent.md#isvalid)
* [render](_validation_index_d_.validationcomponent.md#render)
* [setState](_validation_index_d_.validationcomponent.md#setstate)
* [shouldComponentUpdate](_validation_index_d_.validationcomponent.md#shouldcomponentupdate)
* [validate](_validation_index_d_.validationcomponent.md#validate)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new ValidationComponent**(props: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*): [ValidationComponent](_validation_index_d_.validationcomponent.md)

⊕ **new ValidationComponent**(props: *[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`*, context?: *`any`*): [ValidationComponent](_validation_index_d_.validationcomponent.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:425*

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |

**Returns:** [ValidationComponent](_validation_index_d_.validationcomponent.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:427*

*__deprecated__*: 

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | [IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP` |
| `Optional` context | `any` |

**Returns:** [ValidationComponent](_validation_index_d_.validationcomponent.md)

___

## Properties

<a id="context"></a>

###  context

**● context**: *`any`*

*Inherited from Component.context*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:425*

If using the new style context, re-declare this in your class to be the `React.ContextType` of your `static contextType`.

```ts
static contextType = MyContext
context!: React.ContextType<typeof MyContext>
```

*__deprecated__*: if used without a type annotation, or without static contextType

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

___
<a id="errors"></a>

###  errors

**● errors**: *[IValidationError](../interfaces/_validation_index_d_.ivalidationerror.md)[]*

*Defined in Validation/index.d.ts:137*

Array with all the errors founded

*__type__*: {IValidationError\[\]}

*__memberof__*: ValidationComponent

___
<a id="fieldname"></a>

### `<Optional>` fieldName

**● fieldName**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:144*

Define the name of the field input

*__type__*: {string}

*__memberof__*: ValidationComponent

___
<a id="locale"></a>

### `<Optional>` locale

**● locale**: *`undefined` \| `string`*

*Defined in Validation/index.d.ts:130*

Define the language to show the messages

*__type__*: {string}

*__memberof__*: ValidationComponent

___
<a id="messages"></a>

###  messages

**● messages**: *[IValidationLocale](../interfaces/_validation_index_d_.ivalidationlocale.md)*

*Defined in Validation/index.d.ts:151*

Array with all messages error

*__type__*: {IValidationLocale}

*__memberof__*: ValidationComponent

___
<a id="props"></a>

###  props

**● props**: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> & `Readonly`<`object`>*

*Inherited from Component.props*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:450*

___
<a id="refs"></a>

###  refs

**● refs**: *`object`*

*Inherited from Component.refs*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:456*

*__deprecated__*: [https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs](https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs)

#### Type declaration

[key: `string`]: `ReactInstance`

___
<a id="rules"></a>

###  rules

**● rules**: *[IValidationRules](../interfaces/_validation_index_d_.ivalidationrules.md)*

*Defined in Validation/index.d.ts:158*

Object to define the rules of field

*__type__*: {IValidationRules}

*__memberof__*: ValidationComponent

___
<a id="state"></a>

###  state

**● state**: *`Readonly`<`SS`>*

*Inherited from Component.state*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:451*

___
<a id="value"></a>

###  value

**● value**: *[TypeValidationValue](../modules/_validation_index_d_.md#typevalidationvalue)*

*Defined in Validation/index.d.ts:165*

Define the value of the field

*__type__*: {TypeValidationValue}

*__memberof__*: ValidationComponent

___
<a id="contexttype"></a>

### `<Static>``<Optional>` contextType

**● contextType**: *`Context`<`any`>*

*Inherited from Component.contextType*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:410*

If set, `this.context` will be set at runtime to the current value of the given Context.

Usage:

```ts
type MyContext = number
const Ctx = React.createContext<MyContext>(0)

class Foo extends React.Component {
  static contextType = Ctx
  context!: React.ContextType<typeof Ctx>
  render () {
    return <>My context's value: {this.context}</>;
  }
}
```

*__see__*: [https://reactjs.org/docs/context.html#classcontexttype](https://reactjs.org/docs/context.html#classcontexttype)

___

## Methods

<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:638*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:670*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*, nextState: *`Readonly`<`SS`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:698*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |
| nextState | `Readonly`<`SS`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="__processprops"></a>

### `<Private>` __processProps

▸ **__processProps**(): `void`

*Defined in Validation/index.d.ts:239*

Method to process the props

*__memberof__*: ValidationComponent

**Returns:** `void`

___
<a id="_adderror"></a>

### `<Private>` _addError

▸ **_addError**(fieldName: *`string`*, ruleName: *`string`*, ruleValue: *`any`*): `void`

*Defined in Validation/index.d.ts:224*

Method to add a error

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| fieldName | `string` |  name of the field |
| ruleName | `string` |  name of the rule |
| ruleValue | `any` |  value of the rule |

**Returns:** `void`

___
<a id="_checkrules"></a>

### `<Private>` _checkRules

▸ **_checkRules**(ruleName: *`string`*, ruleValue: *`any`*): `void`

*Defined in Validation/index.d.ts:213*

Method to check rules on a specific field

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| ruleName | `string` |  name of the rule |
| ruleValue | `any` |  value of the rule |

**Returns:** `void`

___
<a id="_defaultmessages"></a>

### `<Private>` _defaultMessages

▸ **_defaultMessages**(): [IValidationLocale](../interfaces/_validation_index_d_.ivalidationlocale.md)

*Defined in Validation/index.d.ts:247*

Method to define the messages error by default

*__memberof__*: ValidationComponent

**Returns:** [IValidationLocale](../interfaces/_validation_index_d_.ivalidationlocale.md)

___
<a id="_defaultrules"></a>

### `<Private>` _defaultRules

▸ **_defaultRules**(): [IValidationRules](../interfaces/_validation_index_d_.ivalidationrules.md)

*Defined in Validation/index.d.ts:255*

Method to define the validation rules by default

*__memberof__*: ValidationComponent

**Returns:** [IValidationRules](../interfaces/_validation_index_d_.ivalidationrules.md)

___
<a id="_reseterrors"></a>

### `<Private>` _resetErrors

▸ **_resetErrors**(): `void`

*Defined in Validation/index.d.ts:231*

Reset error field

*__memberof__*: ValidationComponent

**Returns:** `void`

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:567*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:546*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*, prevState: *`Readonly`<`SS`>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:609*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |
| prevState | `Readonly`<`SS`> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:624*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:653*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:562*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*, nextState: *`Readonly`<`SS`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:683*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |
| nextState | `Readonly`<`SS`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="forceupdate"></a>

###  forceUpdate

▸ **forceUpdate**(callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.forceUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:442*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="geterror"></a>

###  getError

▸ **getError**(ruleName: *`string`*): `string` \| `null`

*Defined in Validation/index.d.ts:203*

Find and return the field rule error

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| ruleName | `string` |  name of the rule |

**Returns:** `string` \| `null`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*, prevState: *`Readonly`<`SS`>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:603*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |
| prevState | `Readonly`<`SS`> |

**Returns:** `SS` \| `null`

___
<a id="haserror"></a>

###  hasError

▸ **hasError**(ruleName: *`string`*): `boolean`

*Defined in Validation/index.d.ts:188*

Method to check if the field rule has error

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| ruleName | `string` |  name of the rule |

**Returns:** `boolean`

___
<a id="haserrors"></a>

###  hasErrors

▸ **hasErrors**(): `boolean`

*Defined in Validation/index.d.ts:180*

Method to check if the field has errors

*__memberof__*: ValidationComponent

**Returns:** `boolean`

___
<a id="isvalid"></a>

###  isValid

▸ **isValid**(): `boolean`

*Defined in Validation/index.d.ts:195*

Method to check if the field is valid

*__memberof__*: ValidationComponent

**Returns:** `boolean`

___
<a id="render"></a>

###  render

▸ **render**(): `ReactNode`

*Inherited from Component.render*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:443*

**Returns:** `ReactNode`

___
<a id="setstate"></a>

###  setState

▸ **setState**<`K`>(state: *`function` \| `null` \| `S` \| `object`*, callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.setState*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:437*

**Type parameters:**

#### K :  `keyof SS`
**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `function` \| `null` \| `S` \| `object` |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`>*, nextState: *`Readonly`<`SS`>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Core/node_modules/@types/react/index.d.ts:557*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IValidationProps](../interfaces/_validation_index_d_.ivalidationprops.md) & `PP`> |
| nextState | `Readonly`<`SS`> |
| nextContext | `any` |

**Returns:** `boolean`

___
<a id="validate"></a>

###  validate

▸ **validate**(rules: *[IValidationInputRules](../interfaces/_validation_index_d_.ivalidationinputrules.md)*): `boolean`

*Defined in Validation/index.d.ts:173*

Method to validate to verify if input respect the validation rules

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| rules | [IValidationInputRules](../interfaces/_validation_index_d_.ivalidationinputrules.md) |  Object to define the rules of the input |

**Returns:** `boolean`

___

