[@ticmakers-react-native/core](../README.md) > ["Helpers/AppHelper"](../modules/_helpers_apphelper_.md) > [AppHelper](../classes/_helpers_apphelper_.apphelper.md)

# Class: AppHelper

Class to define the app helper

*__class__*: AppHelper

## Hierarchy

**AppHelper**

## Index

### Methods

* [isClassComponent](_helpers_apphelper_.apphelper.md#isclasscomponent)
* [isComponent](_helpers_apphelper_.apphelper.md#iscomponent)
* [isCompositeElement](_helpers_apphelper_.apphelper.md#iscompositeelement)
* [isDOMElement](_helpers_apphelper_.apphelper.md#isdomelement)
* [isElement](_helpers_apphelper_.apphelper.md#iselement)
* [isFunctionComponent](_helpers_apphelper_.apphelper.md#isfunctioncomponent)
* [isInstanceComponent](_helpers_apphelper_.apphelper.md#isinstancecomponent)
* [isReactComponent](_helpers_apphelper_.apphelper.md#isreactcomponent)

---

## Methods

<a id="isclasscomponent"></a>

###  isClassComponent

▸ **isClassComponent**(component: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:14*

Method to check if an object is a class component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="iscomponent"></a>

###  isComponent

▸ **isComponent**(component: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:101*

Method to check if an object is a component valid

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="iscompositeelement"></a>

###  isCompositeElement

▸ **isCompositeElement**(element: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:91*

Method to check if an object is a composite element component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| element | `any` |  \- |

**Returns:** `boolean`

___
<a id="isdomelement"></a>

###  isDOMElement

▸ **isDOMElement**(element: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:81*

Method to check if an object is a DOM element component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| element | `any` |  \- |

**Returns:** `boolean`

___
<a id="iselement"></a>

###  isElement

▸ **isElement**(element: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:71*

Method to check if an object is an element component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| element | `any` |  \- |

**Returns:** `boolean`

___
<a id="isfunctioncomponent"></a>

###  isFunctionComponent

▸ **isFunctionComponent**(component: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:24*

Method to check if an object is a function component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="isinstancecomponent"></a>

###  isInstanceComponent

▸ **isInstanceComponent**(component: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:34*

Method to check if an object is a instance component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___
<a id="isreactcomponent"></a>

###  isReactComponent

▸ **isReactComponent**(component: *`any`*): `boolean`

*Defined in Helpers/AppHelper.tsx:61*

Method to check if an object is a react component

*__memberof__*: AppHelper

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| component | `any` |  \- |

**Returns:** `boolean`

___

