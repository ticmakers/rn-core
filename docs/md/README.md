
TIC Makers - React Native Core
==============================

React native component for core.

Powered by [TIC Makers](https://ticmakers.com)

Demo
----

Core Expo's snack

Install
-------

Install `@ticmakers-react-native/core` package and save into `package.json`:

NPM

```shell
$ npm install @ticmakers-react-native/core --save
```

Yarn

```shell
$ yarn add @ticmakers-react-native/core
```

How to use?
-----------

Properties
----------

Name

Type

Default Value

Definition

name

\-

\-

\-

Todo
----

*   Test on iOS
*   Improve and add new features
*   Add more styles
*   Improve readme (example & demo)
*   Create tests

Version 1.0.4 ([Changelog](https://bitbucket.org/ticmakers/rn-core/src/master/CHANGELOG.md))
--------------------------------------------------------------------------------------------

## Index

### External modules

* ["Helpers/AppHelper"](modules/_helpers_apphelper_.md)
* ["Helpers/index.d"](modules/_helpers_index_d_.md)
* ["Validation/ValidationComponent"](modules/_validation_validationcomponent_.md)
* ["Validation/index.d"](modules/_validation_index_d_.md)
* ["Validation/locales/en"](modules/_validation_locales_en_.md)
* ["Validation/locales/es"](modules/_validation_locales_es_.md)
* ["Validation/locales/fa"](modules/_validation_locales_fa_.md)
* ["Validation/locales/fr"](modules/_validation_locales_fr_.md)
* ["index"](modules/_index_.md)

---

