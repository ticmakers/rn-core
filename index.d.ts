import * as React from 'react'
import { ViewStyle, RegisteredStyle, StyleProp, RecursiveArray } from 'react-native'

import AppHelper from './src/Helpers/AppHelper'
import {
  IValidationError,
  IValidationInputRules,
  IValidationLocale,
  IValidationMessages,
  IValidationProps,
  IValidationRules,
  TypeValidationValue,
  ValidationComponent
} from './src/Validation'

/**
 * Type of component to typing components in the Input component
 */
export type TypeComponent =
  JSX.Element
  | React.ComponentClass
  | React.ReactElement
  | false
  | null
  | undefined

/**
 * Type to define the prop style of the Input component
 */
export type TypeStyle =
  false
  | ViewStyle
  | RegisteredStyle<ViewStyle>
  | RecursiveArray<false | ViewStyle | RegisteredStyle<ViewStyle> | null | undefined>
  | StyleProp<ViewStyle>[]
  | null
  | undefined

/**
 * Declaration for Core module
 */
declare module '@ticmakers-react-native/core'

export {
  AppHelper,

  IValidationError,
  IValidationInputRules,
  IValidationLocale,
  IValidationMessages,
  IValidationProps,
  IValidationRules,
  TypeValidationValue,
  ValidationComponent,
}
