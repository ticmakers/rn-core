# TIC Makers - React Native Core
React native component for core.

Powered by [TIC Makers](https://ticmakers.com)

## Demo

[Core Expo's snack]()

## Install

Install `@ticmakers-react-native/core` package and save into `package.json`:

NPM
```shell
$ npm install @ticmakers-react-native/core --save
```

Yarn
```shell
$ yarn add @ticmakers-react-native/core
```

## How to use?

```javascript
```

## Properties

| Name | Type | Default Value | Definition |
| ---- | ---- | ------------- | ---------- |
| name | - | - | -

## Todo

- Test on iOS
- Improve and add new features
- Add more styles
- Improve readme (example & demo)
- Create tests

## Version 1.0.4 ([Changelog])

[Changelog]: https://bitbucket.org/ticmakers/rn-core/src/master/CHANGELOG.md
