"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var moment_1 = __importDefault(require("moment"));
var en_1 = __importDefault(require("./locales/en"));
var es_1 = __importDefault(require("./locales/es"));
var fa_1 = __importDefault(require("./locales/fa"));
var fr_1 = __importDefault(require("./locales/fr"));
var ValidationComponent = (function (_super) {
    __extends(ValidationComponent, _super);
    function ValidationComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.errors = [];
        _this.messages = _this.rules = {};
        _this.value = null;
        _this.__processProps();
        return _this;
    }
    ValidationComponent.prototype.validate = function (rules) {
        this._resetErrors();
        for (var _i = 0, _a = Object.keys(rules); _i < _a.length; _i++) {
            var key = _a[_i];
            var ruleValue = rules[key];
            if (typeof ruleValue !== 'undefined') {
                this._checkRules(key, ruleValue);
            }
        }
        return this.isValid();
    };
    ValidationComponent.prototype.hasErrors = function () {
        return this.errors.length > 0;
    };
    ValidationComponent.prototype.hasError = function (ruleName) {
        var error = this.errors.filter(function (err) { return err.rule === ruleName; })[0];
        return error ? Object.keys(error).length > 0 : false;
    };
    ValidationComponent.prototype.isValid = function () {
        return this.errors.length === 0;
    };
    ValidationComponent.prototype.getError = function (ruleName) {
        var error = this.errors.filter(function (err) { return err.rule === ruleName; })[0];
        if (error) {
            return error.message;
        }
        return null;
    };
    ValidationComponent.prototype._checkRules = function (ruleName, ruleValue) {
        var _rule = this.rules[ruleName];
        var isRuleFunction = (typeof _rule === 'function');
        var isRuleRegExp = (_rule instanceof RegExp);
        var _value = ruleValue;
        if (!_rule) {
            console.warn("The rule \"" + ruleName + "\" isn't defined");
            return;
        }
        if (ruleName === 'date' && typeof _value === 'boolean') {
            _value = 'YYYY-MM-DD';
        }
        if ((isRuleFunction && !_rule(_value, this.value)) || (isRuleRegExp && !_rule.test(this.value))) {
            this._addError(this.fieldName, ruleName, _value);
        }
    };
    ValidationComponent.prototype._addError = function (fieldName, ruleName, ruleValue) {
        if (!this.locale) {
            this.locale = 'en';
        }
        var errMsg = this.messages[this.locale][ruleName].replace('{0}', fieldName).replace('{1}', ruleValue);
        var error = this.errors.filter(function (err) { return err.rule === ruleName; })[0];
        if (error) {
            var index = this.errors.indexOf(error);
            error.message = errMsg;
            this.errors[index] = error;
        }
        else {
            this.errors.push({
                message: errMsg,
                rule: ruleName,
            });
        }
    };
    ValidationComponent.prototype._resetErrors = function () {
        this.errors = [];
    };
    ValidationComponent.prototype.__processProps = function () {
        var _a = this.props, deviceLocale = _a.deviceLocale, messages = _a.messages;
        this.locale = deviceLocale || 'en';
        this.rules = this._defaultRules();
        this.messages = messages || this._defaultMessages();
    };
    ValidationComponent.prototype._defaultMessages = function () {
        return {
            en: en_1.default,
            es: es_1.default,
            fa: fa_1.default,
            fr: fr_1.default,
        };
    };
    ValidationComponent.prototype._defaultRules = function () {
        return {
            date: function (format, value) {
                if (format === void 0) { format = 'YYYY-MM-DD'; }
                var d = moment_1.default(value, format);
                if (d == null || !d.isValid())
                    return false;
                return true;
            },
            email: /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/,
            maxLength: function (length, value) {
                if (length === void (0)) {
                    throw new Error('ERROR: It is not a valid length, checkout your maxlength settings.');
                }
                else if (value.length > length) {
                    return false;
                }
                return true;
            },
            minLength: function (length, value) {
                if (length === void (0)) {
                    throw new Error('ERROR: It is not a valid length, checkout your minlength settings.');
                }
                else if (value.length >= length) {
                    return true;
                }
                return false;
            },
            numbers: /^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/,
            required: function (format, value) {
                return !!value && /\S+/.test(value);
            },
        };
    };
    return ValidationComponent;
}(React.Component));
exports.default = ValidationComponent;
//# sourceMappingURL=ValidationComponent.js.map