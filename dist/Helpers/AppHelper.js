"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var AppHelper = (function () {
    function AppHelper() {
    }
    AppHelper.prototype.isClassComponent = function (component) {
        return typeof component === 'function' && !!component.prototype.isReactComponent;
    };
    AppHelper.prototype.isFunctionComponent = function (component) {
        return typeof component === 'function' && String(component).includes('return React.createElement');
    };
    AppHelper.prototype.isInstanceComponent = function (component) {
        if (typeof component === 'undefined')
            return false;
        var condOwner = typeof component._owner !== 'undefined'
            && typeof component._owner.constructor !== 'undefined'
            && typeof component._owner.constructor.name !== 'undefined';
        var condTypeof = typeof component.$$typeof !== 'undefined'
            && typeof component.$$typeof.constructor !== 'undefined'
            && typeof component.$$typeof.constructor.name !== 'undefined';
        var condComponent = typeof component.type !== 'undefined'
            && typeof component.type.name !== 'undefined'
            && typeof component.ref !== 'undefined';
        return (condOwner && condTypeof && condComponent);
    };
    AppHelper.prototype.isReactComponent = function (component) {
        return this.isClassComponent(component) || this.isFunctionComponent(component) || this.isInstanceComponent(component);
    };
    AppHelper.prototype.isElement = function (element) {
        return React.isValidElement(element);
    };
    AppHelper.prototype.isDOMElement = function (element) {
        return this.isElement(element) && typeof element.type === 'string';
    };
    AppHelper.prototype.isCompositeElement = function (element) {
        return this.isElement(element) && typeof element === 'function';
    };
    AppHelper.prototype.isComponent = function (component) {
        return this.isReactComponent(component) || (this.isDOMElement(component) || this.isCompositeElement(component));
    };
    return AppHelper;
}());
exports.default = new AppHelper();
//# sourceMappingURL=AppHelper.js.map