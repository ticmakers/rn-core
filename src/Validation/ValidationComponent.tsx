import * as React from 'react'
import moment from 'moment'

// Declarations
import {
  IValidationError,
  IValidationInputRules,
  IValidationLocale,
  IValidationProps,
  IValidationRules,
  TypeValidationValue,
} from './index'

// Locales
import localeEn from './locales/en'
import localeEs from './locales/es'
import localeFa from './locales/fa'
import localeFr from './locales/fr'

/**
 * Component base to add validations
 * @class ValidationComponent
 * @extends {(React.Component<IValidationProps & PP, SS>)}
 */
export default class ValidationComponent<PP, SS> extends React.Component<IValidationProps & PP, SS> {
  /**
   * Define the language to show the messages
   * @type {string}
   * @memberof ValidationComponent
   */
  public locale?: string

  /**
   * Array with all the errors founded
   * @type {IValidationError[]}
   * @memberof ValidationComponent
   */
  public errors: IValidationError[]

  /**
   * Define the name of the field input
   * @type {string}
   * @memberof ValidationComponent
   */
  public fieldName?: string

  /**
   * Array with all messages error
   * @type {IValidationLocale}
   * @memberof ValidationComponent
   */
  public messages: IValidationLocale

  /**
   * Object to define the rules of field
   * @type {IValidationRules}
   * @memberof ValidationComponent
   */
  public rules: IValidationRules

  /**
   * Define the value of the field
   * @type {TypeValidationValue}
   * @memberof ValidationComponent
   */
  public value: TypeValidationValue

  /**
   * Creates an instance of ValidationComponent.
   * @param {*} props props of the component
   * @memberof ValidationComponent
   */
  constructor(props: any) {
    super(props)

    this.errors = []
    this.messages = this.rules = {}
    this.value = null
    this.__processProps()
  }

  /**
   * Method to validate to verify if input respect the validation rules
   * @param {IValidationInputRules} rules Object to define the rules of the input
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public validate(rules: IValidationInputRules): boolean {
    // Reset errors
    this._resetErrors()

    for (const key of Object.keys(rules)) {
      const ruleValue = (rules as any)[key]

      if (typeof ruleValue !== 'undefined') {
        this._checkRules(key, ruleValue)
      }
    }

    return this.isValid()
  }

  /**
   * Method to check if the field has errors
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public hasErrors(): boolean {
    return this.errors.length > 0
  }

  /**
   * Method to check if the field rule has error
   * @param {string} ruleName name of the rule
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public hasError(ruleName: string): boolean {
    const [error] = this.errors.filter(err => err.rule === ruleName)
    return error ? Object.keys(error).length > 0 : false
  }

  /**
   * Method to check if the field is valid
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public isValid(): boolean {
    return this.errors.length === 0
  }

  /**
   * Find and return the field rule error
   * @author (Set the text for this tag by adding docthis.authorName to your settings file.)
   * @param {string} ruleName name of the rule
   * @returns {(string | null)}
   * @memberof ValidationComponent
   */
  public getError(ruleName: string): string | null {
    const [error] = this.errors.filter(err => err.rule === ruleName)

    if (error) {
      return error.message
    }

    return null
  }

  /**
   * Method to check rules on a specific field
   * @private
   * @param {string} ruleName name of the rule
   * @param {*} ruleValue value of the rule
   * @returns {void}
   * @memberof ValidationComponent
   */
  private _checkRules(ruleName: string, ruleValue: any): void {
    const _rule = (this.rules as any)[ruleName]
    const isRuleFunction = (typeof _rule === 'function')
    const isRuleRegExp = (_rule instanceof RegExp)
    // const isValueRegExp = (ruleValue instanceof RegExp)
    let _value = ruleValue

    if (!_rule) {
      console.warn(`The rule "${ruleName}" isn't defined`)
      return
    }

    if (ruleName === 'date' && typeof _value === 'boolean') {
      _value = 'YYYY-MM-DD'
    }

    if ((isRuleFunction && !_rule(_value, this.value)) || (isRuleRegExp && !_rule.test(this.value))) {
      this._addError(this.fieldName as string, ruleName, _value)
    }
  }

  /**
   * Method to add a error
   * @private
   * @param {string} fieldName name of the field
   * @param {string} ruleName name of the rule
   * @param {*} ruleValue value of the rule
   * @returns {void}
   * @memberof ValidationComponent
   */
  private _addError(fieldName: string, ruleName: string, ruleValue: any): void {
    if (!this.locale) { this.locale = 'en' }
    const errMsg = (this.messages as any)[this.locale][ruleName].replace('{0}', fieldName).replace('{1}', ruleValue)
    const [error] = this.errors.filter(err => err.rule === ruleName)

    // error already exists
    if (error) {
      // Update existing element
      const index = this.errors.indexOf(error)
      error.message = errMsg
      this.errors[index] = error
    } else {
      // Add new item
      this.errors.push({
        message: errMsg,
        rule: ruleName,
      })
    }
  }

  /**
   * Reset error field
   * @private
   * @memberof ValidationComponent
   */
  private _resetErrors(): void {
    this.errors = []
  }

  /**
   * Method to process the props
   * @private
   * @returns {void}
   * @memberof ValidationComponent
   */
  private __processProps(): void {
    const { deviceLocale, messages } = this.props

    this.locale = deviceLocale || 'en'
    this.rules = this._defaultRules()
    this.messages = messages || this._defaultMessages()
  }

  /**
   * Method to define the messages error by default
   * @private
   * @returns {IValidationLocale}
   * @memberof ValidationComponent
   */
  private _defaultMessages(): IValidationLocale {
    return {
      en: localeEn,
      es: localeEs,
      fa: localeFa,
      fr: localeFr,
    }
  }

  /**
   * Method to define the validation rules by default
   * @private
   * @returns {IValidationRules}
   * @memberof ValidationComponent
   */
  private _defaultRules(): IValidationRules {
    return {
      date(format = 'YYYY-MM-DD', value: any) {
        const d = moment(value, format)
        if (d == null || !d.isValid()) return false
        return true
      },

      email: /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/,

      maxLength(length: number, value: any) {
        if (length === void(0)) {
          throw new Error('ERROR: It is not a valid length, checkout your maxlength settings.')
        } else if (value.length > length) {
          return false
        }

        return true
      },

      minLength(length: number, value: any) {
        if (length === void(0)) {
          throw new Error('ERROR: It is not a valid length, checkout your minlength settings.')
        } else if (value.length >= length) {
          return true
        }

        return false
      },

      numbers: /^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/,

      required(format: RegExp, value: any) {
        return !!value && /\S+/.test(value)
      },
    }
  }
}
