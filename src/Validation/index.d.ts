import * as React from 'react'

/**
 * Type to define the value of the Validation component
 */
export type TypeValidationValue =
  false
  | string
  | number
  | Date
  | object
  | any[]
  | null
  | undefined

/**
 * Interface to define the validation rules
 * @interface IValidationRules
 */
export interface IValidationRules {
  date?: (format: string, value: any) => boolean
  email?: RegExp | string
  maxLength?: (length: number, value: any) => boolean
  minLength?: (length: number, value: any) => boolean
  numbers?: RegExp | string
  required?: (format: RegExp, value: any) => boolean
}

/**
 * Interface to define the validation messages error
 * @interface IValidationMessages
 */
export interface IValidationMessages {
  date?: string
  email?: string
  maxLength?: string
  minLength?: string
  numbers?: string
  pattern?: string
  required?: string
}

/**
 * Interface to define the validation input rules
 * @interface IValidationInputRules
 */
export interface IValidationInputRules {
  date?: boolean | string | RegExp
  email?: boolean | RegExp
  maxLength?: number
  minLength?: number
  numbers?: boolean | RegExp
  pattern?: RegExp
  required?: true
}

/**
 * Interface to define the validation locale messages
 * @interface IValidationLocale
 */
export interface IValidationLocale {
  en?: IValidationMessages
  es?: IValidationMessages
  fr?: IValidationMessages
  fa?: IValidationMessages
}

/**
 * Interface to define the validation message error
 * @interface IValidationError
 */
export interface IValidationError {
  rule: string
  message: string
}

/**
 * Interface to define the props of the Validation component
 * @interface IValidationProps
 */
export interface IValidationProps {
  /**
   * Define de locale to show the messages error
   * @type {string}
   * @memberof IValidationProps
   * @default en
   */
  deviceLocale?: string

  /**
   * Define a list of errors
   * @type {IValidationProps}
   * @memberof IValidationProps
   */
  errors?: IValidationError[]

  /**
   * Define the name of the field
   * @type {string}
   * @memberof IValidationProps
   */
  fieldName?: string

  /**
   * Define the messages error to show
   * @type {object}
   * @memberof IValidationProps
   */
  messages?: object

  /**
   * Define the rules methods by default
   * @type {object}
   * @memberof IValidationProps
   */
  rules?: object
}

/**
 * Component base to add validations
 * @class ValidationComponent
 * @extends {(React.Component<IValidationProps & PP, SS>)}
 */
declare class ValidationComponent<PP, SS> extends React.Component<IValidationProps & PP, SS> {
  /**
   * Define the language to show the messages
   * @type {string}
   * @memberof ValidationComponent
   */
  public locale?: string

  /**
   * Array with all the errors founded
   * @type {IValidationError[]}
   * @memberof ValidationComponent
   */
  public errors: IValidationError[]

  /**
   * Define the name of the field input
   * @type {string}
   * @memberof ValidationComponent
   */
  public fieldName?: string

  /**
   * Array with all messages error
   * @type {IValidationLocale}
   * @memberof ValidationComponent
   */
  public messages: IValidationLocale

  /**
   * Object to define the rules of field
   * @type {IValidationRules}
   * @memberof ValidationComponent
   */
  public rules: IValidationRules

  /**
   * Define the value of the field
   * @type {TypeValidationValue}
   * @memberof ValidationComponent
   */
  public value: TypeValidationValue

  /**
   * Method to validate to verify if input respect the validation rules
   * @param {IValidationInputRules} rules Object to define the rules of the input
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public validate(rules: IValidationInputRules): boolean

  /**
   * Method to check if the field has errors
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public hasErrors(): boolean

  /**
   * Method to check if the field rule has error
   * @param {string} ruleName name of the rule
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public hasError(ruleName: string): boolean

  /**
   * Method to check if the field is valid
   * @returns {boolean}
   * @memberof ValidationComponent
   */
  public isValid(): boolean

  /**
   * Find and return the field rule error
   * @param {string} ruleName name of the rule
   * @returns {(string | null)}
   * @memberof ValidationComponent
   */
  public getError(ruleName: string): string | null

  /**
   * Method to check rules on a specific field
   * @private
   * @param {string} ruleName name of the rule
   * @param {*} ruleValue value of the rule
   * @returns {void}
   * @memberof ValidationComponent
   */
  private _checkRules(ruleName: string, ruleValue: any): void

  /**
   * Method to add a error
   * @private
   * @param {string} fieldName name of the field
   * @param {string} ruleName name of the rule
   * @param {*} ruleValue value of the rule
   * @returns {void}
   * @memberof ValidationComponent
   */
  private _addError(fieldName: string, ruleName: string, ruleValue: any): void

  /**
   * Reset error field
   * @private
   * @memberof ValidationComponent
   */
  private _resetErrors(): void

  /**
   * Method to process the props
   * @private
   * @returns {void}
   * @memberof ValidationComponent
   */
  private __processProps(): void

  /**
   * Method to define the messages error by default
   * @private
   * @returns {IValidationLocale}
   * @memberof ValidationComponent
   */
  private _defaultMessages(): IValidationLocale

  /**
   * Method to define the validation rules by default
   * @private
   * @returns {IValidationRules}
   * @memberof ValidationComponent
   */
  private _defaultRules(): IValidationRules
}

export { ValidationComponent }
