const es = {
  date:          'El campo "{0}" debe ser una fecha válida ({1}).',
  email:         'El campo "{0}" debe ser un correo electrónico válido.',
  maxLength:     'El campo "{0}" debe ser una longitud inferior a {1}.',
  minLength:     'El campo "{0}" debe ser una longitud superior a {1}.',
  numbers:       'El campo "{0}" debe ser un numero válido.',
  required:      'El campo "{0}" es obligatorio.',
}

export default es
