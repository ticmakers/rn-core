const en = {
  date:         'The field "{0}" must be a valid date ({1}).',
  email:        'The field "{0}" must be a valid email address.',
  maxLength:    'The field "{0}" length must be lower than {1}.',
  minLength:    'The field "{0}" length must be greater than {1}.',
  numbers:      'The field "{0}" must be a valid number.',
  required:     'The field "{0}" is required.',
}

export default en
