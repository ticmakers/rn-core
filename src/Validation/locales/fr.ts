const fr = {
  date:          'Le champ "{0}" doit correspondre à une date valide ({1}).',
  email:         'Le champ "{0}" doit être une adresse email valide.',
  maxLength:     'Le nombre de caractère du champ "{0}" doit être inférieur à {1}.',
  minLength:     'Le nombre de caractère du champ "{0}" doit être supérieur à {1}.',
  numbers:       'Le champ "{0}" doit être un nombre valide.',
  required:      'Le champ "{0}" est obligatoire.',
}

export default fr
