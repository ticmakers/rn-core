/**
 * Class to define the app helper
 * @class AppHelper
 */
declare class AppHelper {
  /**
   * Method to check if an object is a class component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isClassComponent(component: any): boolean

  /**
   * Method to check if an object is a function component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isFunctionComponent(component: any): boolean

  /**
   * Method to check if an object is a instance component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isInstanceComponent(component: any): boolean

  /**
   * Method to check if an object is a react component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isReactComponent(component: any): boolean

  /**
   * Method to check if an object is an element component
   * @param {*} element
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isElement(element: any): boolean

  /**
   * Method to check if an object is a DOM element component
   * @param {*} element
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isDOMElement(element: any): boolean

  /**
   * Method to check if an object is a composite element component
   * @param {*} element
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isCompositeElement(element: any): boolean

  /**
   * Method to check if an object is a component valid
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isComponent(component: any): boolean
}

export { AppHelper }
