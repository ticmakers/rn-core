import * as React from 'react'

/**
 * Class to define the app helper
 * @class AppHelper
 */
class AppHelper {
  /**
   * Method to check if an object is a class component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isClassComponent(component: any): boolean {
    return typeof component === 'function' && !!component.prototype.isReactComponent
  }

  /**
   * Method to check if an object is a function component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isFunctionComponent(component: any): boolean {
    return typeof component === 'function' && String(component).includes('return React.createElement')
  }

  /**
   * Method to check if an object is a instance component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isInstanceComponent(component: any): boolean {
    if (typeof component === 'undefined') return false
    const condOwner =
      typeof component._owner !== 'undefined'
      && typeof component._owner.constructor !== 'undefined'
      && typeof component._owner.constructor.name !== 'undefined'

    const condTypeof =
      typeof component.$$typeof !== 'undefined'
      && typeof component.$$typeof.constructor !== 'undefined'
      && typeof component.$$typeof.constructor.name !== 'undefined'

    const condComponent =
      typeof component.type !== 'undefined'
      && typeof component.type.name !== 'undefined'
      && typeof component.ref !== 'undefined'
      // && typeof component.props !== 'undefined'

    return (condOwner && condTypeof && condComponent)
  }

  /**
   * Method to check if an object is a react component
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isReactComponent(component: any): boolean {
    return this.isClassComponent(component) || this.isFunctionComponent(component) || this.isInstanceComponent(component)
  }

  /**
   * Method to check if an object is an element component
   * @param {*} element
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isElement(element: any): boolean {
    return React.isValidElement(element)
  }

  /**
   * Method to check if an object is a DOM element component
   * @param {*} element
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isDOMElement(element: any): boolean {
    return this.isElement(element) && typeof element.type === 'string'
  }

  /**
   * Method to check if an object is a composite element component
   * @param {*} element
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isCompositeElement(element: any): boolean {
    return this.isElement(element) && typeof element === 'function'
  }

  /**
   * Method to check if an object is a component valid
   * @param {*} component
   * @returns {boolean}
   * @memberof AppHelper
   */
  public isComponent(component: any): boolean {
    return this.isReactComponent(component) || (this.isDOMElement(component) || this.isCompositeElement(component))
  }
}

/**
 * Export default instance for AppHelper
 */
export default new AppHelper()
