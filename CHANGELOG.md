# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Released

## [1.0.4] - 2019-05-24

### Fixed

- Fix dependencies version

## [1.0.3] - 2019-05-09

### Added

- Added declarations for components and styles

### Fixed

- Improve validation required for the ValidationComponent

## [1.0.2] - 2019-05-02

### Changes

- Improve export declarations

## [1.0.1] - 2019-04-30

### Added

- Added section to add class helpers
- Added class AppHelper

## [1.0.0] - 2019-04-05

### Release

### Added

- Added docs api

# Unreleased

## [1.0.0-beta.7] - 2019-04-04

### Fixed

- Fix exports in index


## [1.0.0-beta.6] - 2019-04-04

### Fixed

- Changes files locales from json to ts

## [1.0.0-beta.5] - 2019-04-04

### Added

- Added back the folder dist

### Fixed

- Removed postinstall script from npm because has strange behavior
- Fix export of ValidationComponent

## [1.0.0-beta.4] - 2019-04-04

### Added

- Added script postinstall to npm

## [1.0.0-beta.3] - 2019-04-04

### Fixed

- Fix declarations to export correctly

### Removed

- Folder dist was ignored and deleted

## [1.0.0-beta.2] - 2019-04-04

### Fixed

- Fix export and import to class ValidationComponent

## [1.0.0-beta.1] - 2019-04-04

### Fixed

- Fix export and import to class ValidationComponent

## [1.0.0-beta.0] - 2019-04-04

### Added

- Upload and publish first version

[1.0.2]: https://bitbucket.org/ticmakers/rn-core/src/v1.0.1/
[1.0.1]: https://bitbucket.org/ticmakers/rn-core/src/v1.0.1/
[1.0.0]: https://bitbucket.org/ticmakers/rn-core/src/v1.0.0/
[1.0.0-beta.7]: #
[1.0.0-beta.6]: #
[1.0.0-beta.5]: #
[1.0.0-beta.4]: #
[1.0.0-beta.3]: #
[1.0.0-beta.2]: #
[1.0.0-beta.1]: #
[1.0.0-beta.0]: #
